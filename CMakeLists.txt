cmake_minimum_required(VERSION 2.8)
include(build/build.cmake)
include(build/doxygen.cmake)

# インクルードディレクトリ設定
include_directories(${CMAKE_SOURCE_DIR}/external/googletest/googletest/include)
include_directories(${CMAKE_SOURCE_DIR}/external/googletest/googlemock/include)
include_directories(${CMAKE_SOURCE_DIR}/src)

# ライブラリディレクトリ設定
link_directories(${CMAKE_SOURCE_DIR}/lib)

# ビルドターゲット設定
# ライブラリ
add_subdirectory(external/googletest)

# ソースコード
add_subdirectory(src)

# テストコード
enable_testing()
add_subdirectory(test)