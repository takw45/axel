//
// Copyright (c) 2016 takw45
//
#include <tuple>

#include <gtest/gtest.h>

#include <graphics/vector2.h>

using axl::graphics::Vector2;

typedef std::tuple<int32_t, int32_t, int32_t, int32_t, int32_t, int32_t> test_param_t;
class Vector2Test : public ::testing::TestWithParam<test_param_t>{
 protected:
	virtual void SetUp() {
	}
	virtual void TearDown(){
	}
};

TEST_P(Vector2Test, Add) {
  test_param_t param = GetParam();
  Vector2 origin_vec(std::get<0>(param), std::get<1>(param));
  Vector2 add_vec(std::get<2>(param), std::get<3>(param));

  Vector2 vector = origin_vec+add_vec;
  ASSERT_EQ(vector.x_, std::get<4>(param));
  ASSERT_EQ(vector.y_, std::get<5>(param));
}

test_param_t test_data[] = {
    std::make_tuple(0, 0, 10, 10, 10, 10),
    std::make_tuple(5, 9, 39, 40, 44, 49),
};
INSTANTIATE_TEST_CASE_P(
  Add,
  Vector2Test,
  testing::ValuesIn(test_data)
);
