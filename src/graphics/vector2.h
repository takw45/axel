//
// Copyright (c) 2016 takw45
//
#ifndef AXL_GRAPHICS_VECTOR2_H_
#define AXL_GRAPHICS_VECTOR2_H_

#include <axl_types.h>

namespace axl {
namespace graphics {

//! 2次元平面上の座標を扱うクラス
//!
class Vector2 {
 public:
  int32_t x_; //!< X座標
  int32_t y_; //!< Y座標

 public:
  //! コンストラクタ
  //!
  Vector2() : x_(0), y_(0) {
  };

  //! コンストラクタ
  //!
  //! @param x [in] X座標
  //! @param y [in] Y座標
  Vector2(int32_t x, int32_t y) : x_(x), y_(y) {
  };

  //! デストラクタ
  ~Vector2() = default;

  //! ベクトル同士の加算
  //!
  //! @param vec 加算対象のベクトル
  //! @return 自身と引数で渡されたベクトルを加算したベクトル
  inline Vector2 operator +(const Vector2& vec) const {
    return Vector2(x_+vec.x_, y_+vec.y_);
  };

  //! ベクトル同士の加算
  //!
  //! @param vec 加算対象のベクトル
  //! @return 引数で渡されたベクトルを加算後の自身の参照
  inline Vector2& operator +=(const Vector2& vec) {
    x_+=vec.x_;
    y_+=vec.y_;
    return (*this);
  };

  //! ベクトル同士の減算
  //!
  //! @param vec 減算対象のベクトル
  //! @return 自身から引数で渡されたベクトルを減算したベクトル
  inline Vector2 operator -(const Vector2& vec) const {
    return Vector2(x_-vec.x_, y_-vec.y_);
  };

  //! ベクトル同士の減算
  //!
  //! @param vec 加算対象のベクトル
  //! @return 引数で渡されたベクトルを減算した後の自身の参照
  inline Vector2& operator -=(const Vector2& vec) {
    x_-=vec.x_;
    y_-=vec.y_;
    return (*this);
  };

};

}  // namespace graphics
}  // namespace axl

#endif  // AXL_GRAPHICS_VECTOR2_H_
