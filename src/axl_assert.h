//
// Copyright (c) 2016 takw45
//
#ifndef AXL_ASSERT_H_
#define AXL_ASSERT_H_

#include <stdio.h>
#include <stdlib.h>

#ifndef NDEBUG
#define AXL_ASSERT(expression) \
  if(!(expression)) \
    printf("[%s:%d]%s\n",__FILE__, __LINE__, #expression),abort();
#else
#define AXL_ASSERT(expression)
#endif

#endif  // AXL_ASSERT_H_
