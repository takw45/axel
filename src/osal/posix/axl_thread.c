//
// Copyright (c) 2016 takw45
//
#include "../axl_thread.h"

#include <pthread.h>

#include "axl_assert.h"

//! スレッドを扱う情報の構造体
struct axl_thread_tag {
  pthread_t pthread_handle_;  //!< pthreadハンドル
};

axl_error_t AxlCreateThread(axl_thread_t* thread,
                            void* (*start_proc)(void *), void* arg) {
  int result;
  pthread_t pthread;
  AXL_ASSERT(thread);
  AXL_ASSERT(start_proc);

  result = pthread_create(&pthread, NULL, start_proc, arg);
  if (result == 0) {
    *thread = (axl_thread_t)malloc(sizeof(struct axl_thread_tag));
    (*thread)->pthread_handle_ = pthread;
    return kSuccess;
  }
  return kUnknown;
}

axl_error_t AxlJoinThread(axl_thread_t thread) {
  AXL_ASSERT(thread);
  int result = pthread_join(thread->pthread_handle_, NULL);
  if (result == 0) {
    return kSuccess;
  }
  return kUnknown;
}

void AxlDestroyThread(axl_thread_t thread) {
  AXL_ASSERT(thread);
  free(thread);
}
