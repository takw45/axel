//
// Copyright (c) 2016 takw45
//
#ifndef AXL_OSAL_TYPEDEF_H_
#define AXL_OSAL_TYPEDEF_H_

#include <axl_types.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  kSuccess = 0,
  kInvalidParm,
  kInvalidStatus,
  kInvalidAccess,
  kTimeout,
  kAlreadyExists,
  kOutOfBounds,
  kNotEnoughResource,
  kUnknown,
} axl_error_t;

#ifdef __cplusplus
}
#endif

#endif  // AXL_OSAL_TYPEDEF_H_
