//
// Copyright (c) 2016 takw45
//
#ifndef AXL_OSAL_THREAD_H_
#define AXL_OSAL_THREAD_H_

#include <osal/axl_osal_typedef.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct axl_thread_tag* axl_thread_t;

//! スレッドを作成する
//!
//! @param thread     [out] スレッドのハンドル
//! @param start_proc [in]  スレッドで実行するプロシージャ
//! @param arg        [in]  プロシージャに渡す引数
//!
axl_error_t AxlCreateThread(axl_thread_t* thread, void* (*start_proc)(void *),
                             void* arg);

//! スレッドの終了を待つ
//!
//! @param thread     [in] スレッドのハンドル
//!
axl_error_t AxlJoinThread(axl_thread_t thread);

//! スレッドハンドルを破棄する
//!
//! @param thread     [in] スレッドのハンドル
//!
void AxlDestroyThread(axl_thread_t thread);

#ifdef __cplusplus
}
#endif

#endif // AXL_OSAL_THREAD_H_
