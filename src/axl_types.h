//
// Copyright (c) 2016 takw45
//
#ifndef AXL_TYPES_H_
#define AXL_TYPES_H_

#ifdef WIN32
typedef signed char			int8_t;
typedef unsigned char		uint8_t;
typedef signed short		int16_t;
typedef unsigned short		uint16_t;
typedef signed int			int32_t;
typedef unsigned int		uint32_t;
typedef signed long long	int64_t;
typedef unsigned long long	uint64_t;
#else
#include <inttypes.h>
#include <unistd.h>
#endif

#endif  // AXL_TYPES_H_
